// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "amount.h"

#include "tinyformat.h"

#include "policy/policy.h"

const std::string CURRENCY_UNIT = "BOOT";

CFeeRate::CFeeRate(const CAmount& nFeePaid, size_t nBytes_)
{
    //  If parameters are invalid, return a valid response instead of aborting
    if (!(nBytes_ <= uint64_t(std::numeric_limits<int64_t>::max()))) {
        nSatoshisPerK = DEFAULT_INCREMENTAL_RELAY_FEE;
        return;
    }
    // assert(nBytes_ <= uint64_t(std::numeric_limits<int64_t>::max()));

    int64_t nSize = int64_t(nBytes_);

    if (nSize > 0)
        nSatoshisPerK = nFeePaid * 1000 / nSize;
    else
        nSatoshisPerK = 0;
}

CAmount CFeeRate::GetFee(size_t nBytes_) const
{
    //  If parameters are invalid, return a valid response instead of aborting
    if (!(nBytes_ <= uint64_t(std::numeric_limits<int64_t>::max()))) {
        return DEFAULT_INCREMENTAL_RELAY_FEE;
    }
    // assert(nBytes_ <= uint64_t(std::numeric_limits<int64_t>::max()));
    int64_t nSize = int64_t(nBytes_);

    CAmount nFee = nSatoshisPerK * nSize / 1000;

    if (nFee == 0 && nSize != 0) {
        if (nSatoshisPerK > 0)
            nFee = CAmount(1);
        if (nSatoshisPerK < 0)
            nFee = CAmount(-1);
    }

    return nFee;
}

std::string CFeeRate::ToString() const
{
    return strprintf("%d.%08d %s/kB", nSatoshisPerK / COIN, nSatoshisPerK % COIN, CURRENCY_UNIT);
}
